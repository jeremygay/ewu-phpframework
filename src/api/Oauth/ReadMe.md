# EWU PhP Framework Oauth
The purpose of this documentation is to explain how this framework utilizes Oauth, how use Oauth in your application, and how to create custom Oauth clients to expand the list of Authorization servers. The following documentation will be broken down in the following order:
* What is Oauth
* How to create a custom client
* How to Use Oauth

### What is Oauth?
>OAuth is an open standard for access delegation, commonly used as a way for Internet users to grant websites or applications access to their information on other websites but without giving them the passwords. This mechanism is used by companies such as Google, Facebook, Microsoft and Twitter to permit the users to share information about their accounts with third party applications or websites. - [Wikipedia](http://en.wikipedia.org/wiki/OAuth)

Understanding OAuth 2 [terminology](https://oauth2.thephpleague.com/terminology/) is important to know going forward in this documentation. Additional definitions can be found  [here](https://oauth.net/core/1.0/#anchor3) under the Definitions section.

Oauth can be broken up into a three party interaction; the user, the consumer, and the service provider. The process in which the three communicate can be broken down and explained in plain English in this post : [Introduction to OAuth](https://blog.varonis.com/introduction-to-oauth/)

In summary, Oauth, both 1 and 2, is used to give consumer applications access to a user's infomration without having to set up a client log in. This allows for fewer security vulnerabilities, and allows for the user to set which data they wish to share with the consumer.

### How to create a client within our framework
This framework is currently using the [Lusitanian/PHPoAuthLib](https://github.com/Lusitanian/PHPoAuthLib) provider. This library sets up the communication between the client (our Framework/application) and the Authorization server (Google/Facebook/etc). The Lusitanian/PHPoAuthLib library provides [example](https://github.com/Lusitanian/PHPoAuthLib/tree/master/examples) of clients for various authorization servers. 

Documentation for implementing one of these examples is lacking, but below will contain the details used to set up Oauth with one of the example clients.

1) The only thing you will need from the example directory will be the client of your choosing (Google/Facebook/Twitter/etc)'s php file. This php file needs to exists in the /src/api/Oauth/ folder of the framework.

2) You will need to create a Consumer Key and Consumer Secret for the authenication server. 
    * For example, if I were making a client for Google, I would go to  [Google Developers](https://developers.google.com/) and login. Navigate to API Manager, and open the credentials tab. This is where you create credentials for your client so it may communicate with Google.

3) Next open the init.php file found at /src/api/Oauth/init.php within the framework, and find the corresponding service in which you wish to use. Paste the keys in their corresponding fields.
    * (From my previous example I would use google).

4) Now we need to modify the example php file retrieved from step 1.
    * First encapsulate the code (minus requires) into a function. (for an example of this refer to the BitBucketOauth.php file found in the same directory.
    * Next add a return value at the bottom of the function. (return ="";)
    * Within the function find the code that forwards to the autherazation server's URL: 
        * Usually found in the form of "header('Location: URLcode);" or similar.
        * Change the code to "echo '<script>window.location = "'.  URLcode  .'";</script>';"
5) Finally we need to add a route to get to the page in which to log in. Add the following code to the bottom of the file which contains the client code:
    * $app->match('/desiredURI/', $name_of_fuction);
    

### How to use Oauth
// to be continued